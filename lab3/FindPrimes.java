public class FindPrimes {

    public static void main(String[] args){
        int number = Integer.parseInt(args[0]);
		int aim;
		int test;
		boolean control;
		System.out.print("2");
		for (aim = 3; aim <= number ; aim++) 
		{
			control = true;
			for (test = 2; test < aim && control == true ; test++)
			{
				if (test != aim)
				{
					if(aim%test == 0)
					{
						control = false;
					}
				}
			}
			if (control)
			{
				System.out.print(","+aim);
			}
		}
    }
}